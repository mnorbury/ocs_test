#!/usr/bin/env python
import pika

connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
channel = connection.channel()

channel.queue_declare(queue='dkist.logmessages', durable=True)

while True:
    logmessage = raw_input("Enter log message: ")
    channel.basic_publish(exchange='',
                      routing_key='dkist.logmessages',
                      body=logmessage,
    properties=pika.BasicProperties(
                         delivery_mode = 2, # make message persistent
                      ))
connection.close()
