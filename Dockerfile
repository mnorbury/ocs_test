FROM rabbitmq:3.5-management

RUN rabbitmq-plugins enable rabbitmq_shovel rabbitmq_shovel_management

COPY rabbitmq.config /etc/rabbitmq/rabbitmq.config

CMD ["rabbitmq-server"]
