#!/usr/bin/env python
import pika
from collections import defaultdict
from termcolor import cprint
import json

color_map = defaultdict(lambda: 'green', {
   "dkist.experiments": "green",
   "dkist.directives": "red"
})

connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
channel = connection.channel()

#channel.queue_declare(queue='hello')

def callback(ch, method, properties, body):
    cprint(method.routing_key+"="*80, color_map[method.routing_key])
    cprint(json.dumps(json.loads(body), indent=4), color_map[method.routing_key])

queues = ['dkist.experiments','dkist.directives']
for queue in queues:
    channel.basic_consume(callback,
                          queue=queue,
                          no_ack=True)

print(' [*] Waiting for messages. To exit press CTRL+C')
channel.start_consuming()
